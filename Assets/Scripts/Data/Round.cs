﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class Round  {

    public string roundName;
    public bool isNegativeValue;

    public Round(string name, bool isNegative)
    {
        roundName = name;
        isNegativeValue = isNegative;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopUpData {

    public string header, body;
    public UIPopUp.ButtonClickCallBack yesButtonClick, noButtonClick;

    public PopUpData(string header, string body)
    {
        this.header = header;
        this.body = body;
    }

    public PopUpData(string header, string body, UIPopUp.ButtonClickCallBack yesButtonClick)
    {
        this.header = header;
        this.body = body;
        this.yesButtonClick = yesButtonClick;
    }

    public PopUpData(string header, string body, UIPopUp.ButtonClickCallBack yesButtonClick, UIPopUp.ButtonClickCallBack noButtonClick)
    {
        this.header = header;
        this.body = body;
        this.yesButtonClick = yesButtonClick;
        this.noButtonClick = noButtonClick;
    }
}

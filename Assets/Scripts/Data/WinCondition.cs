﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class WinCondition
{
    public Game.WinConditionOption winCondition;
    public int helperValue;

    public WinCondition(Game.WinConditionOption condition, int value)
    {
        winCondition = condition;
        helperValue = value;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class Player {

    public readonly string playerName;
    public List<int> scores = new List<int>();
    public int position;

    public int CurrentTotal {
        get
        {
            int total = 0;
            foreach (var value in scores)
            {
                if(value > -1)
                    total += value;
            }
            return total;
        }
    }

    public Player(string name)
    {
        playerName = name;
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class GameResult  {

    public string gameName;
    public List<List<int>> scores;

    public GameResult(string name, List<List<int>> scores)
    {
        gameName = name;
        this.scores = scores;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class Game  {

    public string name;
    public int spriteIndex;

    public List<Round> rounds;   

    public enum ScoringStyleOption { FixedValues, FreeValues, }
    public ScoringStyle scoringStyle;

    public enum WinConditionOption { LeastPoints, MostPoints, FirstToValue }
    public WinCondition winCondition;

    public Game(WinCondition winCondition, ScoringStyle scoringStyle, List<Round> rounds, string name, int spriteIndex)
    {
        this.name = name;
        this.rounds = rounds;
        this.spriteIndex = spriteIndex;
        this.winCondition = winCondition;
        this.scoringStyle = scoringStyle;
    }

    public Game(WinCondition winCondition, ScoringStyle scoringStyle, string name, int spriteIndex)
    {
        this.winCondition = winCondition;
        this.scoringStyle = scoringStyle;
        this.name = name;
        this.spriteIndex = spriteIndex;
    }
}

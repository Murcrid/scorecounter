﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class ScoringStyle
{
    public Game.ScoringStyleOption scoringStyle;
    public int helperValue;

    public ScoringStyle(Game.ScoringStyleOption style, int value)
    {
        scoringStyle = style;
        helperValue = value;
    }
}

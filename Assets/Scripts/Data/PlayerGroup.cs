﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class PlayerGroup  {

    public string groupName;
    public List<Player> players;
    public List<GameResult> results = new List<GameResult>();
    public int spriteIndex;

    public PlayerGroup(string groupName, List<Player> players, int spriteIndex)
    {
        this.groupName = groupName;
        this.players = players;
        this.spriteIndex = spriteIndex;
    }

    public PlayerGroup(string groupName, List<Player> players)
    {
        this.groupName = groupName;
        this.players = players;
    }
}

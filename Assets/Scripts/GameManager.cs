﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public class GameManager : MonoBehaviour {

    [SerializeField] internal List<PlayerGroup> playerGroups = null;
    [SerializeField] internal List<Game> games = null;
    [SerializeField] internal List<Sprite> playerSprites = null;
    [SerializeField] internal List<Sprite> gameSprites = null;

    [SerializeField] private GameObject[] UIElements = null;
    [SerializeField] private Transform uiMainParent = null, uiAssistParent = null;
    
    internal Game activeGame;
    internal PlayerGroup activePlayerGroup;

    public enum UIWindows { Lobby, Game, HighScores, CreateGameType, SelectGameType, EditGameType, CreatePlayerGroup, SelectPlayerGroup, EditPlayerGroup, PopUp }

    private UIWindows activeWindowType, previousWindowType;
    private GameObject activeMainUIWindow, activeAssistUIWindow;

    public void Start()
    {
        playerGroups = LoadPlayerGroups();
        games = GetGameTypes();
        ShowWindow(UIWindows.Lobby);
    }

    internal void FinnishGame()
    {
        List<List<int>> resultList = new List<List<int>>();

        foreach (var player in activePlayerGroup.players)
        {
            resultList.Add(player.scores);
        }
        activePlayerGroup.results.Add(new GameResult(activeGame.name, resultList));
        SavePlayerGroup(activePlayerGroup);

        ShowWindow(UIWindows.Lobby);
    }

    internal Sprite GetPlayerSprite(int spriteIndex)
    {        
        return playerSprites[spriteIndex];
    }

    internal Sprite GetGameSprite(int spriteIndex)
    {
        return gameSprites[spriteIndex];
    }

    #region Window Management

    public void ShowWindow_EditGameType(Game game)
    {
        ShowWindow(UIWindows.EditGameType);
    }

    public void ShowWindow_EditPlayerGroup(PlayerGroup group)
    {
        ShowWindow(UIWindows.EditPlayerGroup);
        activeMainUIWindow.GetComponent<UI_Window>().Init(this, group);
    }

    public void ShowWindow(UIWindows window)
    {
        if ((window == UIWindows.Game || window == UIWindows.HighScores) && (activeGame == null || activePlayerGroup == null))
            return;
        if (activeMainUIWindow != null)
            Destroy(activeMainUIWindow);

        previousWindowType = activeWindowType;
        activeWindowType = window;

        activeMainUIWindow = Instantiate(GetUIElementWithName(window.ToString()), uiMainParent);
        activeMainUIWindow.GetComponent<UI_Window>().Init(this);
    }

    public void ShowWindow_PopUpInfo(PopUpData data)
    {
        if (activeAssistUIWindow != null)
            Destroy(activeAssistUIWindow);
        activeAssistUIWindow = Instantiate(GetUIElementWithName("UIPopUpInfo"), uiAssistParent);
        activeAssistUIWindow.GetComponent<UIPopUp>().Init(data);
    }

    public void ShowWindow_PreviousWindow()
    {
        if (activeWindowType != previousWindowType)
            ShowWindow(previousWindowType);
    }

    private GameObject GetUIElementWithName(string name)
    {
        foreach (var element in UIElements)
        {
            if (element.name.Contains(name))
                return element;
        }
        Debug.LogError("Couldn't find an element with given name: " + name);
        return null;
    }

    #endregion
    #region Save/Load
    internal void SaveGameType(Game game)
    {
        if (!Directory.Exists(Application.persistentDataPath + "/Games"))
            Directory.CreateDirectory(Application.persistentDataPath + "/Games");
        else if (!File.Exists(Application.persistentDataPath + "/Games/" + game.name))
            games.Add(game);
        else
            File.Delete(Application.persistentDataPath + "/Games/" + game.name);

        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/Games/" + game.name);
        bf.Serialize(file, game);
        file.Close();
    }

    internal void DeleteGameType(Game game)
    {
        if (!Directory.Exists(Application.persistentDataPath + "/Games"))
            Directory.CreateDirectory(Application.persistentDataPath + "/Games");
        else if (File.Exists(Application.persistentDataPath + "/Games/" + game.name))
            File.Delete(Application.persistentDataPath + "/Games/" + game.name);

        if(games.Contains(game))
            games.Remove(game);

        if(activeWindowType == UIWindows.SelectGameType)
            ShowWindow(UIWindows.SelectGameType);
    }

    internal List<Game> GetGameTypes()
    {
        if (!Directory.Exists(Application.persistentDataPath + "/Games"))
            Directory.CreateDirectory(Application.persistentDataPath + "/Games");
        DirectoryInfo dir = new DirectoryInfo(Application.persistentDataPath + "/Games");
        List<Game> lists = new List<Game>();
        FileInfo[] info = dir.GetFiles();
        foreach (FileInfo f in info)
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/Games/" + f.Name, FileMode.Open);
            lists.Add(((Game)bf.Deserialize(file)));
            file.Close();
        }
        return lists;
    }


    internal void SavePlayerGroup(PlayerGroup group)
    {
        if (!Directory.Exists(Application.persistentDataPath + "/PlayerLists"))
            Directory.CreateDirectory(Application.persistentDataPath + "/PlayerLists");
        else if (!File.Exists(Application.persistentDataPath + "/PlayerLists/" + group.groupName))
            playerGroups.Add(group);
        else
            File.Delete(Application.persistentDataPath + "/PlayerLists/" + group.groupName);
            
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/PlayerLists/" + group.groupName);
        bf.Serialize(file, group);
        file.Close();
    }

    internal void DeletePlayerGroup(PlayerGroup group)
    {
        if (!Directory.Exists(Application.persistentDataPath + "/PlayerLists"))
            Directory.CreateDirectory(Application.persistentDataPath + "/PlayerLists");
        else if (File.Exists(Application.persistentDataPath + "/PlayerLists/" + group.groupName))
            File.Delete(Application.persistentDataPath + "/PlayerLists/" + group.groupName);

        if(playerGroups.Contains(group))
            playerGroups.Remove(group);

        if (activeWindowType == UIWindows.SelectPlayerGroup)
            ShowWindow(UIWindows.SelectPlayerGroup);
    }

    internal List<PlayerGroup> LoadPlayerGroups()
    {
        if (!Directory.Exists(Application.persistentDataPath + "/PlayerLists"))
            Directory.CreateDirectory(Application.persistentDataPath + "/PlayerLists");
        DirectoryInfo dir = new DirectoryInfo(Application.persistentDataPath + "/PlayerLists");
        List<PlayerGroup> lists = new List<PlayerGroup>();
        FileInfo[] info = dir.GetFiles();
        foreach (FileInfo f in info)
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/PlayerLists/" + f.Name, FileMode.Open);
            lists.Add(((PlayerGroup)bf.Deserialize(file)));
            file.Close();
        }
        return lists;
    }
    #endregion
}

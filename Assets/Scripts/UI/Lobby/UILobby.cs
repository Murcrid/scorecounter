﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UILobby : UI_Window {

    public override void Init(GameManager gameManager)
    {
        base.Init(gameManager);
    }

    public void ButtonClick_PlayGame()
    {
        gameManager.ShowWindow(GameManager.UIWindows.SelectGameType);
    }

    public void ButtonClick_NewPlayerList()
    {
        gameManager.ShowWindow(GameManager.UIWindows.CreatePlayerGroup);
    }

    public void ButtonClick_NewGameType()
    {
        gameManager.ShowWindow(GameManager.UIWindows.CreateGameType);
    }

    public override void ButtonClick_Info()
    {
        throw new System.NotImplementedException();
    }
}

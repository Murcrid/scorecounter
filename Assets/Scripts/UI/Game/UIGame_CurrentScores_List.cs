﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class UIGame_CurrentScores_List : UIGame_ScoreStyle_Base {

    [SerializeField] private TextMeshProUGUI playerNameField = null, totalField = null, positionField = null;
    
    [SerializeField] private Transform roundFieldParent = null;
    [SerializeField] private Slider slider = null;
    [SerializeField] private GameObject roundFieldPrefab = null;

    private UIGame_CurrentScores_List_RoundScoreField[] roundFields;

    private GameManager gameManager;
    private int maxObjectsShown = 5;
    private bool wasInitialized;

    public override void Init(GameManager gameManager, Player player)
    {
        if (!wasInitialized)
        {
            this.gameManager = gameManager;
            this.player = player;

            playerNameField.text = player.playerName;
            totalField.text = player.CurrentTotal.ToString();
            positionField.text = player.position.ToString();

            roundFields = new UIGame_CurrentScores_List_RoundScoreField[gameManager.activeGame.rounds.Count];

            if (roundFields.Length <= maxObjectsShown)
            {
                slider.gameObject.SetActive(false);
                roundFieldParent.GetComponent<VerticalLayoutGroup>().padding.left += 40;
            }
            else
            {
                slider.gameObject.SetActive(true);
                slider.maxValue = roundFields.Length - maxObjectsShown;
            }
            for (int i = 0; i < roundFields.Length; i++)
            {
                roundFields[i] = (Instantiate(roundFieldPrefab, roundFieldParent).GetComponent<UIGame_CurrentScores_List_RoundScoreField>());
                roundFields[i].Init(gameManager, player, i);
                if (i > maxObjectsShown - 1)
                    roundFields[i].gameObject.SetActive(false);
            }
        }
        else
            UpdateScoreLists();
        wasInitialized = true;
    }

    private void UpdateScoreLists()
    {
        int sliderValue = int.Parse(slider.value.ToString());
        totalField.text = player.CurrentTotal.ToString();
        positionField.text = player.position.ToString();

        for (int i = 0; i < roundFields.Length; i++)
        {
            if (i < sliderValue || i >= sliderValue + maxObjectsShown)
            {
                roundFields[i].gameObject.SetActive(false);
            }
            else
            {
                roundFields[i].gameObject.SetActive(true);
                roundFields[i].Init(gameManager, player, i);
            }
        }
    }

    public void OnSliderValueChanged()
    {
        UpdateScoreLists();
    }

    public override void OnPointerDown(PointerEventData eventData)
    {
        FindObjectOfType<UI_Game>().ModifyScoreList(this);
        base.OnPointerDown(eventData);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UIGame_CurrentScores_List_RoundScoreField : MonoBehaviour {

    [SerializeField] private TextMeshProUGUI scoreField = null, roundCountField = null;

    private int index;
    private Player player;

    public int GetScore()
    {
        return player.scores[index];
    }

    public void Init(GameManager gameManager, Player player ,int index)
    {
        roundCountField.text = gameManager.activeGame.rounds[index].roundName;
        SetScoreText(player.scores[index]);
        this.index = index;
        this.player = player;
    }

    public void SetScoreText(int score)
    {
        if (score > -1)
            scoreField.text = score.ToString();
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;

public class UI_Game : UI_Window
{
    [SerializeField] private TextMeshProUGUI nameField = null;
    [SerializeField] private GameObject scores_Current = null, scores_ModifyPrefab = null;
    [SerializeField] private Transform scores_ModifyParent = null;

    private UIGame_CurrentScores_List_AddScores scores_Modify = null;

    public override void Init(GameManager gameManager)
    {
        nameField.text = gameManager.activeGame.name;
        base.Init(gameManager);
        InitializePlayerScoreLists();
        scores_Current.GetComponent<UIGame_Scores_Current>().Init(gameManager);
    }

    #region ButtonClicks
    public void ButtonClick_Finnish()
    {
        gameManager.FinnishGame();
    }

    public void ButtonClick_Quit()
    {
        gameManager.ShowWindow(GameManager.UIWindows.Lobby);
    }
    public override void ButtonClick_Info()
    {
        throw new NotImplementedException();
    }
    #endregion

    private void InitializePlayerScoreLists()
    {
        foreach (var player in gameManager.activePlayerGroup.players)
        {
            player.scores = new List<int>();
            for (int i = 0; i < gameManager.activeGame.rounds.Count; i++)
            {
                player.scores.Add(-1);
            }
            if (gameManager.activeGame.rounds.Count == 0)
                player.scores.Add(0);
        }
    }

    internal void ModifyScoreList(UIGame_CurrentScores_List scoreListToModify)
    {
        if (scores_Modify != null)
            Destroy(scores_Modify.gameObject);

        scores_Modify = Instantiate(scores_ModifyPrefab, scores_ModifyParent).GetComponent<UIGame_CurrentScores_List_AddScores>();
        scores_Modify.Init(gameManager, scoreListToModify.player);
    }
}

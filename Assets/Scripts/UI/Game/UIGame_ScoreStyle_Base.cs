﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class UIGame_ScoreStyle_Base : Selectable {

    public abstract void Init(GameManager gameManager, Player player);
    internal Player player;
    internal int originalPosition;

}

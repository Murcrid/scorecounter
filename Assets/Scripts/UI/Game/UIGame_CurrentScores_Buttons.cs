﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UIGame_CurrentScores_Buttons : UIGame_ScoreStyle_Base
{
    [SerializeField] private TextMeshProUGUI nameText = null, positionText = null, scoreText = null, addButtonText = null, subtractButtonText = null;

    private int targetScore, increment;
    private GameManager gameManager;

    public override void Init(GameManager gameManager, Player player)
    {
        this.gameManager = gameManager;
        this.player = player;

        nameText.text = player.playerName;
        positionText.text = player.position.ToString();

        targetScore = gameManager.activeGame.winCondition.helperValue;
        increment = gameManager.activeGame.scoringStyle.helperValue;

        addButtonText.text = "Add " + increment;
        subtractButtonText.text = "Subtract " + increment;

        UpdateFields();
    }

    private void UpdateFields()
    {
        if (gameManager.activeGame.winCondition.winCondition == Game.WinConditionOption.FirstToValue)
            scoreText.text = player.scores[0] + "/" + targetScore;
        else
            scoreText.text = player.scores[0].ToString();
    }

    public void AddScore()
    {
        if (gameManager.activeGame.winCondition.winCondition != Game.WinConditionOption.FirstToValue || player.scores[0] < targetScore)
            player.scores[0]++;
        UpdateFields();
    }

    public void SubtractScore()
    {
        if(player.scores[0] > 0)
            player.scores[0]--;
        UpdateFields();
    }
}

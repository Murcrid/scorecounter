﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UIGame_CurrentScores_List_AddScores_RoundScoreField : MonoBehaviour {

    [SerializeField] private TextMeshProUGUI roundCountField;
    [SerializeField] private TMP_InputField scoreInput;

    public int GetScore()
    {
        int value = -1;
        if (scoreInput.text != "") 
            int.TryParse(scoreInput.text,  out value);
        return value;
    }

    public void Init(GameManager gameManager, Player player, int index)
    {
        roundCountField.text = "Round \n" + (index + 1);
        if (player.scores[index] > -1)
            scoreInput.text = player.scores[index].ToString();
    }

    public void SetScore(int score)
    {
        scoreInput.text = score.ToString();
    }
}

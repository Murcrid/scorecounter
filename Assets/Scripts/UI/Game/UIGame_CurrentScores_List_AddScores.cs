﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIGame_CurrentScores_List_AddScores : MonoBehaviour {

    [SerializeField] private TextMeshProUGUI playerNameField;

    [SerializeField] private Transform bigRoundFieldParent = null;
    [SerializeField] private Slider slider = null;
    [SerializeField] private GameObject bigRoundFieldPrefab = null;

    private UIGame_CurrentScores_List_AddScores_RoundScoreField[] bigRoundFields;

    private GameManager gameManager;
    private int maxObjectsShown = 5;
    private Player player;

    internal void Init(GameManager gameManager, Player player)
    {
        this.gameManager = gameManager;
        this.player = player;

        playerNameField.text = player.playerName;

        bigRoundFields = new UIGame_CurrentScores_List_AddScores_RoundScoreField[gameManager.activeGame.rounds.Count];

        if (bigRoundFields.Length <= maxObjectsShown)
        {
            slider.gameObject.SetActive(false);
            bigRoundFieldParent.GetComponent<VerticalLayoutGroup>().padding.left += 40;
        }
        else
        {
            slider.gameObject.SetActive(true);
            slider.maxValue = bigRoundFields.Length - maxObjectsShown;
        }

        for (int i = 0; i < bigRoundFields.Length; i++)
        {
            bigRoundFields[i] = (Instantiate(bigRoundFieldPrefab, bigRoundFieldParent).GetComponent<UIGame_CurrentScores_List_AddScores_RoundScoreField>());
            bigRoundFields[i].Init(gameManager, player, i);
            if (i > maxObjectsShown - 1)
                bigRoundFields[i].gameObject.SetActive(false);
        }
    }

    private void UpdateScoreLists()
    {
        int sliderValue = int.Parse(slider.value.ToString());

        if (bigRoundFields.Length > maxObjectsShown)
        {
            for (int i = 0; i < bigRoundFields.Length; i++)
            {
                if (i < sliderValue || i >= sliderValue + maxObjectsShown)
                {
                    bigRoundFields[i].gameObject.SetActive(false);
                }
                else
                {
                    bigRoundFields[i].gameObject.SetActive(true);
                }
            }
        }
    }

    public void OnSliderValueChanged()
    {
        UpdateScoreLists();
    }

    public void ButtonClick_Return()
    {    
        int i = 0;
        foreach (var field in bigRoundFields)
        {
            player.scores[i++] = field.GetScore();
        }

        Destroy(gameObject);
        FindObjectOfType<UIGame_Scores_Current>().Init(gameManager);
    }
}


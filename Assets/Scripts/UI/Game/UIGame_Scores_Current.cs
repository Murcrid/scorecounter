﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIGame_Scores_Current : MonoBehaviour {

    [SerializeField] private Transform scoreListParent = null;
    [SerializeField] private Slider slider = null;
    [SerializeField] private GameObject playerScoreListPrefab = null, playerScoreIncrementPrefab = null;

    private UIGame_ScoreStyle_Base[] playerScores;

    private GameManager gameManager;
    private int maxObjectsShown = 4;
    private bool wasInitialized;

    internal void Init(GameManager gameManager)
    {
        if (!wasInitialized)
        {
            this.gameManager = gameManager;

            playerScores = new UIGame_ScoreStyle_Base[gameManager.activePlayerGroup.players.Count];
            if (playerScores.Length <= maxObjectsShown)
            {
                slider.gameObject.SetActive(false);
            }
            else
            {
                slider.gameObject.SetActive(true);
                slider.maxValue = playerScores.Length - maxObjectsShown;
                scoreListParent.GetComponent<GridLayoutGroup>().padding.right += 40;
                scoreListParent.GetComponent<GridLayoutGroup>().cellSize = new Vector2(360, 565);
            }

            GameObject usedPrefab = gameManager.activeGame.scoringStyle.scoringStyle == Game.ScoringStyleOption.FixedValues ? playerScoreIncrementPrefab : playerScoreListPrefab;

            for (int i = 0; i < playerScores.Length; i++)
            {
                playerScores[i] = (Instantiate(usedPrefab, scoreListParent).GetComponent<UIGame_ScoreStyle_Base>());
                gameManager.activePlayerGroup.players[i].position = i + 1;
                playerScores[i].Init(gameManager, gameManager.activePlayerGroup.players[i]);
                if (i > maxObjectsShown - 1)
                    playerScores[i].gameObject.SetActive(false);
            }

        }
        else
            UpdatePlayerPositionsAndScores();

        wasInitialized = true;
    }

    private void UpdatePlayerPositionsAndScores()
    {
        int sliderValue = int.Parse(slider.value.ToString());
        int childCount = scoreListParent.childCount;

        List<UIGame_ScoreStyle_Base> players = new List<UIGame_ScoreStyle_Base>();

        for (int i = 0; i < playerScores.Length; i++)
        {
            players.Add(playerScores[i]);
        }

        if(gameManager.activeGame.winCondition.winCondition == Game.WinConditionOption.LeastPoints)
            players.Sort((x, y) => x.GetComponent<UIGame_ScoreStyle_Base>().player.CurrentTotal.CompareTo(y.GetComponent<UIGame_ScoreStyle_Base>().player.CurrentTotal));
        else
            players.Sort((x, y) => y.GetComponent<UIGame_ScoreStyle_Base>().player.CurrentTotal.CompareTo(x.GetComponent<UIGame_ScoreStyle_Base>().player.CurrentTotal));

        for (int i = 0; i < playerScores.Length; i++)
        {
            playerScores[i] = players[i];
        }

        for (int i = 0; i < playerScores.Length; i++)
        {
            playerScores[i].transform.SetAsLastSibling();
            playerScores[i].player.position = (i + 1);

            if (i < sliderValue || i >= sliderValue + maxObjectsShown)
                playerScores[i].gameObject.SetActive(false);
            else
            {
                playerScores[i].gameObject.SetActive(true);
                playerScores[i].Init(gameManager, players[i].player);
            }
        }
    }

    public void OnSliderValueChanged()
    {
        UpdatePlayerPositionsAndScores();
    }
}
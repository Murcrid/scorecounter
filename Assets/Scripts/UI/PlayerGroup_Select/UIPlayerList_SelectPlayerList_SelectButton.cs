﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.EventSystems;

public class UIPlayerList_SelectPlayerList_SelectButton : MonoBehaviour {

    [SerializeField] private Image myImage = null;
    [SerializeField] private TextMeshProUGUI groupName = null;

    private PlayerGroup myGroup;
    private UIPlayerList_SelectPlayerList mySender;

    public void Init(UIPlayerList_SelectPlayerList sender, PlayerGroup group)
    {
        groupName.text = group.groupName;
        myImage.sprite = FindObjectOfType<GameManager>().GetPlayerSprite(group.spriteIndex);

        mySender = sender;
        myGroup = group;
    }

    #region Button Clicks
    public void ButtonClick_Edit()
    {
        mySender.ChildButtonClick_Edit(myGroup);
    }

    public void ButtonClick_Delete()
    {
        mySender.ChildButtonClick_Delete(myGroup);
    }

    public void ButtonClick_Select()
    {
        mySender.ChildButtonClick_Select(myGroup);
    }
    #endregion
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIPlayerList_SelectPlayerList : UI_Window
{
    [SerializeField] private Transform playerListParent = null;
    [SerializeField] private Slider slider = null;
    [SerializeField] private GameObject playerListPrefab = null;

    private UIPlayerList_SelectPlayerList_SelectButton[] playerListButtons;

    private int maxObjectsShown = 4;

    public override void Init(GameManager gameManager)
    {
        if (!wasInitialized)
        {
            playerListButtons = new UIPlayerList_SelectPlayerList_SelectButton[gameManager.playerGroups.Count];

            if (playerListButtons.Length <= maxObjectsShown)
            {
                slider.gameObject.SetActive(false);
            }
            else
            {
                slider.gameObject.SetActive(true);
                slider.maxValue = playerListButtons.Length - maxObjectsShown;
                playerListParent.GetComponent<VerticalLayoutGroup>().childAlignment = TextAnchor.UpperRight;
            }

            for (int i = 0; i < playerListButtons.Length; i++)
            {
                playerListButtons[i] = (Instantiate(playerListPrefab, playerListParent).GetComponent<UIPlayerList_SelectPlayerList_SelectButton>());
                playerListButtons[i].Init(this, gameManager.playerGroups[i]);
                if (i > maxObjectsShown - 1)
                    playerListButtons[i].gameObject.SetActive(false);
            }
        }
        else
            UpdatePlayerScores();
        wasInitialized = true;
        base.Init(gameManager);
    }

    private void UpdatePlayerScores()
    {
        int sliderValue = int.Parse(slider.value.ToString());

        if (playerListButtons.Length > maxObjectsShown)
        {
            for (int i = 0; i < playerListButtons.Length; i++)
            {
                if (i < sliderValue || i >= sliderValue + maxObjectsShown)
                    playerListButtons[i].gameObject.SetActive(false);
                else
                {
                    playerListButtons[i].gameObject.SetActive(true);
                }
            }
        }
    }

    public void OnSliderValueChanged()
    {
        UpdatePlayerScores();
    }

    public void ChildButtonClick_Edit(PlayerGroup group)
    {
        gameManager.ShowWindow_EditPlayerGroup(group);
    }

    public void ChildButtonClick_Select(PlayerGroup group)
    {
        gameManager.activePlayerGroup = group;
        gameManager.ShowWindow(GameManager.UIWindows.Game);
    }

    public void ChildButtonClick_Delete(PlayerGroup group)
    {
        gameManager.DeletePlayerGroup(group);
    }

    public void ButtonClick_CreateNewPlayerGroup()
    {
        gameManager.ShowWindow(GameManager.UIWindows.CreatePlayerGroup);
    }

    public void ButtonClick_Back()
    {
        gameManager.ShowWindow(GameManager.UIWindows.Lobby);
    }

    public override void ButtonClick_Info()
    {
        throw new NotImplementedException();
    }
}

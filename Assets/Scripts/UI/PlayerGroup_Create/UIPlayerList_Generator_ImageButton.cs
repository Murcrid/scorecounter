﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class UIPlayerList_Generator_ImageButton : Selectable {

    [SerializeField] private Image myImage = null;

    public void SetColor(Color color)
    {
        myImage.color = color;
    }

    public void Init(Sprite sprite)
    {
        myImage.sprite = sprite;
    }

    public override void OnPointerDown(PointerEventData eventData)
    {
        FindObjectOfType<UIPlayerList_Generator_Create>().SelectImage(myImage.sprite);
        base.OnPointerDown(eventData);
    }
}

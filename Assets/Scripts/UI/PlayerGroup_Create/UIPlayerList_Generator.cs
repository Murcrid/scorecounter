﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System;

public class UIPlayerList_Generator : UI_Window {

    public override void ButtonClick_Info()
    {
        throw new NotImplementedException();
    }

    public void ButtonClick_Quit()
    {
        gameManager.ShowWindow_PreviousWindow();
    }

    internal void CreatePlayerList(PlayerGroup myList)
    {
        if(!gameManager.playerGroups.Contains(myList))
            gameManager.SavePlayerGroup(myList);
        gameManager.ShowWindow_PreviousWindow();
    }
}

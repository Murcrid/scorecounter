﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIPlayerList_Generator_Create : MonoBehaviour {

    [SerializeField] private TMP_InputField groupNameInput = null, playerCountInput = null;
    [SerializeField] private Toggle giveNamesToggle = null, useDefaultsToggle = null;
    [SerializeField] private GameObject nameQuestions = null, listCreate = null, spritePrefab = null;
    [SerializeField] private Transform spriteParent = null;
    [SerializeField] private Slider slider = null;

    private GameManager gameManager;
    private int spriteIndex = -1;
    private int numberOfPlayers;
    private string groupName;
    private List<GameObject> spriteButtons = new List<GameObject>();
    private int maxNumberofSprites = 5;

    #region Sprite Buttons
    private void Start()
    {
        gameManager = FindObjectOfType<GameManager>();
        List<Sprite> sprites = gameManager.playerSprites;

        int i = 0;
        slider.maxValue = sprites.Count - maxNumberofSprites;
        foreach (var sprite in sprites)
        {            
            spriteButtons.Add(Instantiate(spritePrefab, spriteParent));
            spriteButtons[i].GetComponent<UIPlayerList_Generator_ImageButton>().Init(sprite);
            if (i > maxNumberofSprites - 1)
                spriteButtons[i].SetActive(false);
            i++;
        }
    }

    public void OnSliderValueChanged()
    {
        int sliderAsInt = int.Parse(slider.value.ToString());

        for (int i = 0; i < spriteButtons.Count; i++)
        {
            if (i < sliderAsInt || i > sliderAsInt + maxNumberofSprites - 1)
                spriteButtons[i].SetActive(false);
            else
                spriteButtons[i].SetActive(true);
        }
    }

    public void SelectImage(Sprite sprite)
    {
        spriteIndex = gameManager.playerSprites.IndexOf(sprite);
        int i = 0;
        foreach (var spriteButton in spriteButtons)
        {
            if(i == spriteIndex)
                spriteButton.GetComponent<UIPlayerList_Generator_ImageButton>().SetColor(Color.green);
            else
                spriteButton.GetComponent<UIPlayerList_Generator_ImageButton>().SetColor(Color.white);
            i++;
        }
    }
    #endregion

    public void Toggle_GiveNamesChangeValue()
    {
        if (giveNamesToggle.isOn)
            useDefaultsToggle.isOn = false;
        else
            useDefaultsToggle.isOn = true;
    }

    public void Toggle_UseDefaultChangeValue()
    {
        if (useDefaultsToggle.isOn)
            giveNamesToggle.isOn = false;
        else
            giveNamesToggle.isOn = true;
    }

    public void ButtonClick_Confirm()
    {
        numberOfPlayers = int.Parse(playerCountInput.text);
        groupName = groupNameInput.text;

        if (numberOfPlayers > 0 )   //Check that playernumber count is set by user
        {
            if (CheckNameInput()) //Check that name is set by user
            {
                if (spriteIndex > -1) //Check that sprite is set by user
                {
                    if (giveNamesToggle.isOn)
                    {
                        listCreate.SetActive(false);
                        nameQuestions.SetActive(true);
                        nameQuestions.GetComponent<UIPlayerList_Generator_Names>().Init(numberOfPlayers, groupName, spriteIndex);
                    }
                    else
                    {
                        FindObjectOfType<UIPlayerList_Generator>().CreatePlayerList(new PlayerGroup(groupName, GetDefaultNames(), spriteIndex));
                    }
                }
                else
                    gameManager.ShowWindow_PopUpInfo(new PopUpData("Error", "Select image"));
            }
            else
                gameManager.ShowWindow_PopUpInfo(new PopUpData("Error", "Give name"));
        }
        else
            gameManager.ShowWindow_PopUpInfo(new PopUpData("Error", "Give number of players"));
    }

    private bool CheckNameInput()
    {
        if (groupName == "")
            return false;

        foreach (var groups in gameManager.playerGroups)
        {
            if (groups.groupName == groupName)
                return false;
        }
        return true;
    }

    private List<Player> GetDefaultNames()
    {
        List<Player> playerNames = new List<Player>();
        for (int i = 0; i < numberOfPlayers; i++)
        {
            playerNames.Add(new Player("Player " + (i + 1)));
        }
        return playerNames;
    }
}

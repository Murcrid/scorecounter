﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UIPlayerList_Generator_Names : MonoBehaviour
{
    [SerializeField] private TMP_InputField nameInput = null;
    [SerializeField] private TextMeshProUGUI playerCounter = null, nextButtonTxt = null;

    private int numberOfPlayers;
    private string groupName;
    private int spriteIndex;
    private List<Player> players = new List<Player>();
    private bool buttonClicked;

    internal void Init(int numberOfPlayers, string groupName, int spriteIndex)
    {
        this.groupName = groupName;
        this.numberOfPlayers = numberOfPlayers;
        this.spriteIndex = spriteIndex;
        SetPlayerCounterANDButtonText(numberOfPlayers, 1);
        StartCoroutine("GiveNames");
    }

    private IEnumerator GiveNames()
    {
        int playerCount = 1;
        do
        {
            if (buttonClicked)
            {
                if (CheckNameInput())
                {
                    players.Add(new Player(nameInput.text));
                    SetPlayerCounterANDButtonText(numberOfPlayers, ++playerCount);                 
                }
                else
                {
                    //TODO: Create the popup for this to tell what the namefield error was
                }
                buttonClicked = false;
            }
            yield return new WaitForEndOfFrame();
        } while (playerCount <= numberOfPlayers);

        FindObjectOfType<UIPlayerList_Generator>().CreatePlayerList(new PlayerGroup(groupName, players, spriteIndex));
    }

    private bool CheckNameInput()
    {
        if (nameInput.text == "")
            return false;

        foreach (var player in players)
        {
            if (player.playerName == nameInput.text)
                return false;
        }
        return true;
    }

    public void ButtonClick_Next()
    {
        buttonClicked = true;
    }

    private void SetPlayerCounterANDButtonText(int maxPlayers, int currentCount)
    {
        playerCounter.text = "Player " + currentCount + "/" + maxPlayers;
        if (maxPlayers == currentCount)
            nextButtonTxt.text = "Finnish";
    }
}

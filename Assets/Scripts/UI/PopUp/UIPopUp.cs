﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UIPopUp : MonoBehaviour {

    [SerializeField] private TextMeshProUGUI headerTxt = null, bodyTxt = null;
    [SerializeField] private GameObject yesButton = null, noButton = null, continueButton = null;

    public delegate void ButtonClickCallBack();

    private ButtonClickCallBack yesButtonClick, noButtonClick;
    private bool callBackNull = false;

    public void Init(PopUpData popUpData)
    {
        this.noButtonClick = popUpData.noButtonClick;
        this.yesButtonClick = popUpData.yesButtonClick;
        this.headerTxt.text = popUpData.header;
        this.bodyTxt.text = popUpData.body;

        if (noButtonClick != null)
        {
            yesButton.SetActive(true);
            noButton.SetActive(true);
            continueButton.SetActive(false);
        }
        else if(yesButtonClick != null)
        {
            yesButton.SetActive(false);
            noButton.SetActive(false);
            continueButton.SetActive(true);
        }
        else
        {
            yesButton.SetActive(false);
            noButton.SetActive(false);
            continueButton.SetActive(true);

            callBackNull = true;
        }
    }

    #region ButtonClicks
    public void ButtonClick_Yes()
    {
        if (!callBackNull)
            yesButtonClick();
        Destroy(gameObject);
    }

    public void ButtonClick_No()
    {
        if (!callBackNull)
            noButtonClick();
        Destroy(gameObject);
    }
    #endregion
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System;

public class UIGameType_Generator_Create : MonoBehaviour {

    [SerializeField] private TMP_InputField gameName = null, additionalQuestionOneInput = null, additionalQuestionTwoInput = null;
    [SerializeField] private GameObject spritePrefab = null, additionalQuestionOneGO = null, additionalQuestionTwoGO = null;
    [SerializeField] private Transform spriteParent = null;
    [SerializeField] private Slider slider = null;
    [SerializeField] private TMP_Dropdown scoringStyleDropdown = null, winConditionDropdown = null;
    [SerializeField] private TextMeshProUGUI addtionalQuestionOneText = null, addtionalQuestionTwoText = null;

    [SerializeField] private GameObject namesGO;

    private int roundCountInt;

    private GameManager gameManager;
    private int spriteIndex = -1;
    private List<GameObject> spriteButtons = new List<GameObject>();
    private int maxNumberofSprites = 5;

    private void Start()
    {
        gameManager = FindObjectOfType<GameManager>();
        CreateSprites();
        InitializeDropDowns();
    }

    private void InitializeDropDowns()
    {
        scoringStyleDropdown.ClearOptions();
        List<TMP_Dropdown.OptionData> optionDatas = new List<TMP_Dropdown.OptionData>();
        foreach (var style in Enum.GetValues(typeof(Game.ScoringStyleOption)))
        {
            char[] arr = style.ToString().ToCharArray();
            string returned = style.ToString();
            int index = 0;
            for (int i = 0; i < arr.Length; i++)
            {
                if (char.IsUpper(arr[i]) && i != 0)
                {
                    returned = returned.Insert(index++, " ");
                }
                index++;
            }
            optionDatas.Add(new TMP_Dropdown.OptionData(returned));
        }
        scoringStyleDropdown.AddOptions(optionDatas);
        OnScoringStyleValueChanged();

        winConditionDropdown.ClearOptions();
        optionDatas = new List<TMP_Dropdown.OptionData>();
        foreach (var condition in Enum.GetValues(typeof(Game.WinConditionOption)))
        {
            char[] arr = condition.ToString().ToCharArray();
            string returned = condition.ToString();
            int index = 0;
            for (int i = 0; i < arr.Length; i++)
            {
                if(char.IsUpper(arr[i]) && i != 0)
                {
                    returned = returned.Insert(index++, " ");
                }
                index++;
            }
            optionDatas.Add(new TMP_Dropdown.OptionData(returned));
        }
        winConditionDropdown.AddOptions(optionDatas);
        OnWinConditionValueChanged();
    }

    #region Sprite Buttons
    private void CreateSprites()
    {
        List<Sprite> sprites = gameManager.gameSprites;

        int i = 0;
        slider.maxValue = sprites.Count - maxNumberofSprites;
        foreach (var sprite in sprites)
        {
            spriteButtons.Add(Instantiate(spritePrefab, spriteParent));
            spriteButtons[i].GetComponent<UIGameType_Generator_ImageButton>().Init(sprite);
            if (i > maxNumberofSprites - 1)
                spriteButtons[i].SetActive(false);
            i++;
        }
    }

    public void OnSliderValueChanged()
    {
        int sliderAsInt = int.Parse(slider.value.ToString());

        for (int i = 0; i < spriteButtons.Count; i++)
        {
            if (i < sliderAsInt || i > sliderAsInt + maxNumberofSprites - 1)
                spriteButtons[i].SetActive(false);
            else
                spriteButtons[i].SetActive(true);
        }
    }

    public void SelectImage(Sprite sprite)
    {
        spriteIndex = gameManager.gameSprites.IndexOf(sprite);
        int i = 0;
        foreach (var spriteButton in spriteButtons)
        {
            if (i == spriteIndex)
                spriteButton.GetComponent<UIGameType_Generator_ImageButton>().SetColor(Color.green);
            else
                spriteButton.GetComponent<UIGameType_Generator_ImageButton>().SetColor(Color.white);
            i++;
        }
    }
    #endregion

    public void OnScoringStyleValueChanged()
    {
        switch ((Game.ScoringStyleOption)scoringStyleDropdown.value)
        {
            case Game.ScoringStyleOption.FixedValues:
                additionalQuestionOneGO.gameObject.SetActive(true);
                addtionalQuestionOneText.text = "Increment value:";
                break;
            case Game.ScoringStyleOption.FreeValues:
                additionalQuestionOneGO.gameObject.SetActive(true);
                addtionalQuestionOneText.text = "How many rounds?";
                break;
        }
    }

    public void OnWinConditionValueChanged()
    {
        switch ((Game.WinConditionOption)winConditionDropdown.value)
        {
            case Game.WinConditionOption.LeastPoints:
                additionalQuestionTwoGO.gameObject.SetActive(false);
                break;
            case Game.WinConditionOption.MostPoints:
                additionalQuestionTwoGO.gameObject.SetActive(false);
                break;
            case Game.WinConditionOption.FirstToValue:
                additionalQuestionTwoGO.gameObject.SetActive(true);
                addtionalQuestionTwoText.text = "Value to get";
                break;
        }
    }

    public void ButtonClick_Confirm()
    {
        if (CheckNameInput() && CheckAdditionalOneText() && CheckAdditionalTwoText() && CheckSpriteCountText())
        {
            WinCondition win = new WinCondition((Game.WinConditionOption)winConditionDropdown.value, int.Parse(additionalQuestionTwoInput.text == "" ? "-1" : additionalQuestionOneInput.text));
            ScoringStyle score = new ScoringStyle((Game.ScoringStyleOption)scoringStyleDropdown.value, int.Parse(additionalQuestionOneInput.text));

            if ((Game.ScoringStyleOption)scoringStyleDropdown.value == Game.ScoringStyleOption.FixedValues)
            {
                FindObjectOfType<UIGameType_Generator>().CreateGame(new Game(win, score, new List<Round>() ,gameName.text, spriteIndex));
            }
            else
            {
                gameObject.SetActive(false);
                namesGO.SetActive(true);

                namesGO.GetComponent<UIGameType_Generator_Names>().Init(win, score, gameName.text, spriteIndex);
            }
        }
        else
        {
            if (!CheckNameInput())
            {
                gameManager.ShowWindow_PopUpInfo(new PopUpData("Error", "You must name the game."));
            }
            else if(!CheckSpriteCountText())
            {
                gameManager.ShowWindow_PopUpInfo(new PopUpData("Error", "Select an image for your game."));
            }
            else if(CheckAdditionalOneText() || CheckAdditionalTwoText())
            {
                gameManager.ShowWindow_PopUpInfo(new PopUpData("Error", "Fill the additional fields."));
            }
        }

    }

    #region Checks
    private bool CheckSpriteCountText()
    {
        if (spriteIndex < 0 || spriteIndex > gameManager.gameSprites.Count - 1)
            return false;
        return true;
    }

    private bool CheckAdditionalTwoText()
    {
        if (!additionalQuestionTwoGO.gameObject.activeInHierarchy)
            return true;

        if (additionalQuestionTwoInput.text == "")
            return false;

        int i = int.Parse(additionalQuestionTwoInput.text);

        if (i <= 0)
            return false;
        return true;
    }

    private bool CheckAdditionalOneText()
    {
        if (!additionalQuestionOneGO.gameObject.activeInHierarchy)
            return true;

        if (additionalQuestionOneInput.text == "")
            return false;

        int i = int.Parse(additionalQuestionOneInput.text);

        if (i < 0 || i > 20)
            return false;
        return true;
    }

    private bool CheckNameInput()
    {
        if (gameName.text == "")
            return false;

        foreach (var game in FindObjectOfType<GameManager>().games)
        {
            if (game.name == gameName.text)
                return false;
        }
        return true;
    }
    #endregion
}

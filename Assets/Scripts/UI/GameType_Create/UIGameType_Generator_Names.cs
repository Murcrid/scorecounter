﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class UIGameType_Generator_Names : MonoBehaviour {

    [SerializeField] private TMP_InputField nameInput = null;
    [SerializeField] private TextMeshProUGUI roundCounter = null, nextButtonTxt = null;
    [SerializeField] private Toggle isNegativeToggle = null;

    private WinCondition winCondition;
    private ScoringStyle scoringStyle;
    private string gameName;
    private int spriteIndex;
    private List<Round> rounds = new List<Round>();
    private bool buttonClicked;

    internal void Init(WinCondition winCondition, ScoringStyle scoringStyle, string gameName, int spriteIndex)
    {
        this.gameName = gameName;
        this.winCondition = winCondition;
        this.scoringStyle = scoringStyle;
        this.spriteIndex = spriteIndex;
        SetRoundCounterANDButtonText(scoringStyle.helperValue, 1);
        StartCoroutine("GiveNames");
    }

    private IEnumerator GiveNames()
    {
        int index = 1;
        do
        {
            if (buttonClicked)
            {
                if (CheckNameInput())
                {
                    rounds.Add(new Round(nameInput.text, isNegativeToggle.isOn));
                    SetRoundCounterANDButtonText(scoringStyle.helperValue, ++index);
                }
                else
                {
                    //TODO: Create the popup for this to tell what the namefield error was
                }
                buttonClicked = false;
            }
            yield return new WaitForEndOfFrame();
        } while (index <= scoringStyle.helperValue);

        FindObjectOfType<UIGameType_Generator>().CreateGame(new Game(winCondition, scoringStyle, rounds, gameName, spriteIndex));
    }

    private bool CheckNameInput()
    {
        if (nameInput.text == "")
            return false;

        foreach (var round in rounds)
        {
            if (round.roundName == nameInput.text)
                return false;
        }
        return true;
    }

    public void ButtonClick_Next()
    {
        buttonClicked = true;
    }

    private void SetRoundCounterANDButtonText(int roundCount, int currentCount)
    {
        roundCounter.text = "Round " + currentCount + "/" + roundCount;
        if (roundCount == currentCount)
            nextButtonTxt.text = "Finnish";
    }
}

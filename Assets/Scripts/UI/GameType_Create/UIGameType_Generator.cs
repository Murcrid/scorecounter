﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UIGameType_Generator : UI_Window
{
    [SerializeField] private TextMeshProUGUI header;

    public override void ButtonClick_Info()
    {
        throw new System.NotImplementedException();
    }

    public override void Init(GameManager gameManager)
    {
        base.Init(gameManager);
    }

    public void ButtonClicl_Back()
    {
        gameManager.ShowWindow_PreviousWindow();
    }

    internal void CreateGame(Game game)
    {
        if (!gameManager.games.Contains(game))
            gameManager.SaveGameType(game);
        gameManager.ShowWindow_PreviousWindow();
    }
}

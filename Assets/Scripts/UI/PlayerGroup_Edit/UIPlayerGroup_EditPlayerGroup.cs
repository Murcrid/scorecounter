﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class UIPlayerGroup_EditPlayerGroup : UI_Window {

    [SerializeField] private TextMeshProUGUI groupName = null;
    [SerializeField] private TMP_InputField playerCountInput = null;
    [SerializeField] private Toggle giveNamesToggle = null;

    [SerializeField] private GameObject imageButtonPrefab = null;
    [SerializeField] private Transform imageButtonParent = null;
    [SerializeField] private Slider imageButtonSlider = null;

    [SerializeField] private GameObject list_Edit = null, list_Names = null;

    private UIPlayerGroup_EditPlayerGroup_ImageButton[] imageButtons;
    private int maxButtonsShown = 5;

    private PlayerGroup myGroup;

    private int spriteIndex;

    public override void Init(GameManager gameManager, PlayerGroup groupToEdit)
    {
        myGroup = groupToEdit;
        groupName.text = groupToEdit.groupName;
        playerCountInput.text = groupToEdit.players.Count.ToString();
        giveNamesToggle.isOn = groupToEdit.players[0].playerName != "Player 1";

        spriteIndex = myGroup.spriteIndex;

        Sprite[] sprites = gameManager.playerSprites.ToArray();
        imageButtons = new UIPlayerGroup_EditPlayerGroup_ImageButton[sprites.Length];

        if (imageButtons.Length <= maxButtonsShown)
        {
            imageButtonSlider.gameObject.SetActive(false);
        }
        else
        {
            imageButtonSlider.gameObject.SetActive(true);
            imageButtonSlider.maxValue = sprites.Length - maxButtonsShown;
        }

        for (int i = 0; i < imageButtons.Length; i++)
        {
            imageButtons[i] = Instantiate(imageButtonPrefab, imageButtonParent).GetComponent<UIPlayerGroup_EditPlayerGroup_ImageButton>();
            imageButtons[i].Init(this, sprites[i], i);

            if (i > maxButtonsShown)
                imageButtons[i].gameObject.SetActive(false);

            if (i == spriteIndex)
                imageButtons[i].SetColor(Color.green);
        }
        base.Init(gameManager);
    }

    private void UpdateImages()
    {
        int sliderAsInt = Mathf.RoundToInt(imageButtonSlider.value);

        if (imageButtons.Length > maxButtonsShown)
        {
            for (int i = 0; i < imageButtons.Length; i++)
            {
                if (i < sliderAsInt || i >= sliderAsInt + maxButtonsShown)
                    imageButtons[i].gameObject.SetActive(false);
                else
                    imageButtons[i].gameObject.SetActive(true);

                if(spriteIndex == i)
                    imageButtons[i].SetColor(Color.green);
                else
                    imageButtons[i].SetColor(Color.white);
            }
        }
    }

    public void OnSliderValueChanged()
    {
        UpdateImages();
    }

    public void SelectImage(int selectedIndex)
    {
        spriteIndex = selectedIndex;
        UpdateImages();
    }

    public void ButtonClick_ConfirmChanges()
    {
        if (giveNamesToggle.isOn)
        {
            list_Edit.SetActive(false);
            list_Names.SetActive(true);
            list_Names.GetComponent<UIPlayerGroup_EditPlayerGroup_Names>().Init(this, int.Parse(playerCountInput.text));
        }
        else
        {
            UpdatePlayerGroup(DefaultPlayerNames());
        }
    }

    public void UpdatePlayerGroup(List<Player> players)
    {
        myGroup.spriteIndex = spriteIndex;
        myGroup.players = players;

        gameManager.SavePlayerGroup(myGroup);
        gameManager.ShowWindow_PreviousWindow();
    }

    private List<Player> DefaultPlayerNames()
    {
        List<Player> players = new List<Player>();
        for (int i = 0; i < int.Parse(playerCountInput.text.ToString()); i++)
        {
            players.Add(new Player("Player " + (i + 1)));
        }
        return players;
    }

    public void ButtonClick_Back()
    {
        gameManager.ShowWindow_PreviousWindow();
    }

    public override void ButtonClick_Info()
    {
        throw new System.NotImplementedException();
    }
}

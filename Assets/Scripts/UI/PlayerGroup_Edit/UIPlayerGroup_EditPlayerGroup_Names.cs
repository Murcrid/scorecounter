﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;

public class UIPlayerGroup_EditPlayerGroup_Names : MonoBehaviour {

    [SerializeField] private TMP_InputField nameInput = null;
    [SerializeField] private TextMeshProUGUI playerCountText = null, buttonText = null;

    private UIPlayerGroup_EditPlayerGroup mySender;
    private int maxPlayerCount, currentCount = 1;

    private bool buttonClick;

    public void Init(UIPlayerGroup_EditPlayerGroup sender, int playerCount)
    {
        maxPlayerCount = playerCount;
        mySender = sender;

        UpdateTexts();
        StartCoroutine(GetNames());
    }

    private IEnumerator GetNames()
    {
        List<Player> players = new List<Player>();
        do
        {
            if(buttonClick)
            {
                buttonClick = false;
                players.Add(new Player(GetInputText(players, currentCount)));
                currentCount++;
                UpdateTexts();
            }
            yield return new WaitForSeconds(0);
        } while (currentCount <= maxPlayerCount);

        mySender.UpdatePlayerGroup(players);
    }

    private string GetInputText(List<Player> players, int i)
    {
        bool nameFound = false;

        foreach (var player in players)
        {
            if (player.playerName == nameInput.text)
                nameFound = true;
        }

        if (nameInput.text == "")
            return "Player " + i;
        else if (nameFound)
            return "Player " + i;
        else
            return nameInput.text;
    }

    private void UpdateTexts()
    {
        playerCountText.text = "Players: " + currentCount  + "/" + maxPlayerCount;
        if (currentCount == maxPlayerCount)
            buttonText.text = "Finnish";
    }

    public void ButtonClick_Corfirm()
    {
        buttonClick = true;
    }
}

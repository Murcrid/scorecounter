﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class UIPlayerGroup_EditPlayerGroup_ImageButton : Selectable {

    [SerializeField] private Image myImage;

    private UIPlayerGroup_EditPlayerGroup mySender;
    private int myIndex;

    public void Init(UIPlayerGroup_EditPlayerGroup sender, Sprite sprite, int index)
    {
        myImage.sprite = sprite;
        mySender = sender;
        myIndex = index;
    }

    public void SetColor(Color color)
    {
        myImage.color = color;
    }

    public override void OnPointerDown(PointerEventData eventData)
    {
        mySender.SelectImage(myIndex);
        base.OnPointerDown(eventData);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIGameType_SelectGameToPlay : UI_Window {

    [SerializeField] private Transform buttonParent = null;
    [SerializeField] private Slider slider = null;
    [SerializeField] private GameObject buttonPrefab = null;

    private UIGame_SelectGameToPlay_GameSelectionButton[] gameTypeButtons;
    private int maxObjectsShown = 4;

    public override void Init(GameManager gameManager)
    {
        if (!wasInitialized)
        {
            gameTypeButtons = new UIGame_SelectGameToPlay_GameSelectionButton[gameManager.games.Count];

            if (gameTypeButtons.Length <= maxObjectsShown)
            {
                slider.gameObject.SetActive(false);
            }
            else
            {
                slider.gameObject.SetActive(true);
                slider.maxValue = gameTypeButtons.Length - maxObjectsShown;
                buttonParent.GetComponent<VerticalLayoutGroup>().childAlignment = TextAnchor.UpperRight;
            }

            for (int i = 0; i < gameTypeButtons.Length; i++)
            {
                gameTypeButtons[i] = (Instantiate(buttonPrefab, buttonParent).GetComponent<UIGame_SelectGameToPlay_GameSelectionButton>());
                gameTypeButtons[i].Init(this, gameManager.games[i]);
                if (i > maxObjectsShown - 1)
                    gameTypeButtons[i].gameObject.SetActive(false);
            }
        }
        else        
            UpdateValues();
        base.Init(gameManager);
    }


    private void UpdateValues()
    {
        int sliderAsInt = int.Parse(slider.value.ToString());

        if (gameTypeButtons.Length > maxObjectsShown)
        {
            for (int i = 0; i < gameTypeButtons.Length; i++)
            {
                if (i < sliderAsInt || i > sliderAsInt + maxObjectsShown - 1)
                    gameTypeButtons[i].gameObject.SetActive(false);
                else
                    gameTypeButtons[i].gameObject.SetActive(true);
            }
        }
    }
    public void OnSliderValueChanged()
    {
        UpdateValues();
    }

    public void ChildButtonClick_Delete(Game game)
    {
        gameManager.DeleteGameType(game);
    }

    public void ChildButtonClick_Edit(Game game)
    {
        gameManager.ShowWindow_EditGameType(game);
    }

    public void ChildButtonClick_Select(Game game)
    {
        gameManager.activeGame = game;
        gameManager.ShowWindow(GameManager.UIWindows.SelectPlayerGroup);
    }

    public void ButtonClick_ReturnToLobby()
    {
        gameManager.ShowWindow(GameManager.UIWindows.Lobby);
    }

    public void ButtonClick_CreateNew()
    {
        gameManager.ShowWindow(GameManager.UIWindows.CreateGameType);
    }

    public override void ButtonClick_Info()
    {
        Debug.Log("Non implemented info click");
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class UIGame_SelectGameToPlay_GameSelectionButton : MonoBehaviour {

    [SerializeField] private TextMeshProUGUI gameName = null;
    [SerializeField] private Image gameImage = null;

    private Game myGame;
    private UIGameType_SelectGameToPlay mySender;

    public void Init(UIGameType_SelectGameToPlay sender, Game game)
    {
        myGame = game;
        mySender = sender;
        gameName.text = game.name;
        gameImage.sprite = FindObjectOfType<GameManager>().GetGameSprite(game.spriteIndex);
    }

    #region Button Clicks
    public void ButtonClick_Edit()
    {
        mySender.ChildButtonClick_Edit(myGame);
    }

    public void ButtonClick_Delete()
    {
        mySender.ChildButtonClick_Delete(myGame);
    }

    public void ButtonClick_Select()
    {
        mySender.ChildButtonClick_Select(myGame);
    }
    #endregion
}

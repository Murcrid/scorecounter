﻿using UnityEngine;
using System.Collections.Generic;

public class CameraResolution : MonoBehaviour
{
    [SerializeField]
    private float wantedWidth = 9.0f, wantedHeight = 16.0f;

    private int ScreenSizeX = 0;
    private int ScreenSizeY = 0;

    private void RescaleCamera()
    {
        if (Screen.width == ScreenSizeX && Screen.height == ScreenSizeY) return;

        float targetaspect = wantedWidth / wantedHeight;
        float windowaspect = (float)Screen.width / (float)Screen.height;

        float scaleheight = windowaspect / targetaspect;
        Camera camera = GetComponent<Camera>();

        if (scaleheight < 1.0f)
        {
            Rect rect = camera.rect;

            rect.width = 1.0f;
            rect.height = scaleheight;
            rect.x = 0;
            rect.y = (1.0f - scaleheight) / 2.0f;

            camera.rect = rect;
        }
        else
        {
            float scalewidth = 1.0f / scaleheight;

            Rect rect = camera.rect;

            rect.width = scalewidth;
            rect.height = 1.0f;
            rect.x = (1.0f - scalewidth) / 2.0f;
            rect.y = 0;

            camera.rect = rect;
        }

        ScreenSizeX = Screen.width;
        ScreenSizeY = Screen.height;
    }

    void OnPreCull()
    {
        if (Application.isEditor) return;
        Rect currRect = Camera.main.rect;
        Rect newRect = new Rect(0, 0, 1, 1);

        Camera.main.rect = newRect;
        GL.Clear(true, true, Color.black);

        Camera.main.rect = currRect;
    }

    void Start()
    {
        RescaleCamera();
    }
}


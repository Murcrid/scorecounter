﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class UI_Window : MonoBehaviour {

    public abstract void ButtonClick_Info();

    protected bool wasInitialized;
    protected GameManager gameManager;

    protected virtual void Start()
    {
        if (!wasInitialized)
            Debug.LogError("Non Initialized UI_Window!");        
    }

    public virtual void Init(GameManager gameManager)
    {
        wasInitialized = true;
        this.gameManager = gameManager;
    }

    public virtual void Init(GameManager gameManager, Game game)
    {
        wasInitialized = true;
        this.gameManager = gameManager;
    }

    public virtual void Init(GameManager gameManager, PlayerGroup playerGroup)
    {
        wasInitialized = true;
        this.gameManager = gameManager;
    }
}
